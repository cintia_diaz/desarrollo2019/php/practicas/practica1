<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                border: 1px solid black;
                width:100px;
                height: 100px;
                margin: 20px auto;
                font-size: 4em;
                line-height: 100px;
                text-align: center;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <?php
        // tirada de dados (2 dados)
        // cada uno de los numeros en una variable
        // mostrar los dados en pantalla
        // en una caja negra centrada la suma de los dos dados
        // la suma de los dos dados debe realizarlo con una funcion
        // denominada suma
        ?>

        <?php

        /**
         * Suma los dos numeros que le paso
         * @param numero $a
         * @param numero $b
         * @return numero
         */
        function suma($a, $b) {
            $s = $a + $b;
            return $s;
        }

        $numero1 = random_int(1,6);
        $numero2 = random_int(1, 6);
        $suma = suma($numero1, $numero2);
        ?>
        <img src="imgs/<?= $numero1 ?>.svg" alt="">
        <img src="imgs/<?= $numero2 ?>.svg" alt="">
        <div><?= $suma ?></div>
        <?php
        $n=33;
        if(($n%2)==0){
            $par=1;
        }else{
            $par=0;
        }
        
        $par=($n%2==0)?1:0;
        
        
        ?>
        
    </body>
</html>
