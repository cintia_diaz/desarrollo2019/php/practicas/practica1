<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // crear un array con 1 numero, 1 texto , otro array
        // imprimir el valor y el tipo
        $a=[
            23,
            "ejemplo",
            [1,2,3]
        ];
        echo "<li>El dato del primer elemento: $a[0]</li>";
        echo "<li>El tipo de dato del primer elemento es : " . 
                gettype($a[0]) . "</li>";
        ?>
    <li>El dato del segundo elemento: <?= $a[1] ?></li>
    <li>El tipo de dato del segundo elemento es: <?= gettype($a[1])?>  </li>
    <li>El dato del tercer elemento: <?= join(",",$a[2]) ?></li>
    <li>El tipo de dato del tercer elemento es: <?= gettype($a[2])?>  </li>
    </body>
</html>
