<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                border: 1px solid black;
                width:100px;
                height: 100px;
                margin: 20px auto;
                font-size: 4em;
                line-height: 100px;
                text-align: center;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <?php
        // imprima un dado en pantalla 
        // representacion numerica del dado
        ?>
        
        <div>
            <?= random_int(1, 6)?>
        </div>
    </body>
</html>
