<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $texto = "Ejemplo de clase";
        $numeros = [1, 2, 3, 4, 5];
        // Crear h1 y meter el texto dentro
        // crear una tabla de 1 columna y en cada 
        // fila introducir los numeros del array
        ?>

        <h1><?= $texto ?></h1>
        <table>
            <tr>
                <td><?= $numeros[0] ?></td>
            </tr>
            <tr>
                <td><?= $numeros[1] ?></td>
            </tr>
            <tr>
                <td><?= $numeros[2] ?></td>
            </tr>
            <tr>
                <td><?= $numeros[3] ?></td>
            </tr>
            <tr>
                <td><?= $numeros[4] ?></td>
            </tr>
        </table>

    </body>
</html>
