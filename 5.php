<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            div{
                border: 1px solid black;
                width:100px;
                height: 100px;
                margin: 20px auto;
                font-size: 4em;
                line-height: 100px;
                text-align: center;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <?php
        // tirada de dados (2 dados)
        // cada uno de los numeros en una variable
        // mostrar los dados en pantalla
        // en una caja negra centrada la suma de los dos dados
        ?>
        
        <?php
        $numero1=random_int(1, 6);
        $numero2=random_int(1, 6);
        ?>
        
        
        <img src="imgs/<?= $numero1 ?>.svg" alt="">
        <img src="imgs/<?= $numero2 ?>.svg" alt="">
        <div><?= $numero1+$numero2 ?></div>
    </body>
</html>
